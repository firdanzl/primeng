import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  items: MenuItem[];
  images:any;
  source:string='';
  constructor() {
    this.items = []
   }

  ngOnInit(): void {
    this.items = [
      {
        label:'Home'
      },
      {
        label: 'Furniture',
        items: [
          {
            label: 'Bedroom',
            icon: 'pi pi-angle-right',
            
          },
          { label: 'Living Room',
            icon: 'pi pi-desktop'
        },
          { label: 'Dining Room',
            
        },
        ],
      },
      {
        label: 'Shop',
        
      },
      {
        label: 'About Us'
      },
      // {
      //   icon:'pi pi-search'
      // }
    ];

    this.images=[
       "../../assets/Frame 105.png",
       "../../assets/Frame 106.png"
    ]
  }
  responsiveOptions:any[] = [
    {
        breakpoint: '1024px',
        numVisible: 5
    },
    {
        breakpoint: '768px',
        numVisible: 3
    },
    {
        breakpoint: '560px',
        numVisible: 1
    }
];

}
